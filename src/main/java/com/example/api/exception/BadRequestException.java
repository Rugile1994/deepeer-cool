package com.example.api.exception;

import java.io.Serial;

public class BadRequestException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 834381472668788549L;

    public BadRequestException(String message) {
        super(message);
    }
}
