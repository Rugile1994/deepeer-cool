package com.example.api.dto;

import java.util.Date;

public class BusinessError {
    private final String description;
    private final Date timestamp;

    public BusinessError(String description, Date timestamp) {
        this.description = description;
        this.timestamp = timestamp;
    }
}
