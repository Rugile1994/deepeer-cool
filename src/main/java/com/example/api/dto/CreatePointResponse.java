package com.example.api.dto;

public class CreatePointResponse {
    public long savedPointsCount;
    public long invalidPointsCount;

    public CreatePointResponse(long savedPointsCount, long invalidPointsCount) {
        this.savedPointsCount = savedPointsCount;
        this.invalidPointsCount = invalidPointsCount;
    }

    public long getSavedPointsCount() {
        return savedPointsCount;
    }

    public long getInvalidPointsCount() {
        return invalidPointsCount;
    }
}
