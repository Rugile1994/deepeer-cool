package com.example.api.controller;

import com.example.api.dto.CreatePointResponse;
import com.example.api.model.Point;
import com.example.api.service.PointService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PointController {

    private final PointService pointService;

    public PointController(PointService pointService) {
        this.pointService = pointService;
    }

    @PostMapping(value = "/points")
    public ResponseEntity<CreatePointResponse> createPoints(@RequestBody List<Point> points) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(pointService.createPoints(points));
    }
}
