package com.example.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "point")
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private final Double x;
    private final Double y;
    private final String name;
    private final String color;

    public Point(Double x, Double y, String name, String color) {
        this.x = x;
        this.y = y;
        this.name = name;
        this.color = color;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }
}
