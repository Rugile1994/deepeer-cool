package com.example.api.service;

import com.example.api.model.Point;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;

@Service
public class PointValidator {
    Predicate<Point> isValidPoint = p -> p.getX() >= 0 && p.getY() >= 0;
}
