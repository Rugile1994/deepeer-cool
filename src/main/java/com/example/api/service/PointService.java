package com.example.api.service;

import com.example.api.dto.CreatePointResponse;
import com.example.api.exception.BadRequestException;
import com.example.api.model.Point;
import com.example.api.repository.PointRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
public class PointService {

    private final PointRepository pointRepository;
    private final PointValidator pointValidator = new PointValidator();
    private final Function<Point, Point> save = point -> {
        getPointRepository().save(point);
        return point;
    };

    public PointService(PointRepository pointRepository) {
        this.pointRepository = pointRepository;
    }

    private PointRepository getPointRepository() {
        return pointRepository;
    }

    public CreatePointResponse createPoints(List<Point> points) {
        if (points.isEmpty()) {
            throw new BadRequestException("Points list is empty!");
        }

        long validPointsAmount = points.stream()
                .filter(pointValidator.isValidPoint)
                .map(save)
                .count();

        return new CreatePointResponse(validPointsAmount, points.size() - validPointsAmount);
    }
}
