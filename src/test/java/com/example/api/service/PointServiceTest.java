package com.example.api.service;

import com.example.api.dto.CreatePointResponse;
import com.example.api.exception.BadRequestException;
import com.example.api.model.Point;
import com.example.api.repository.PointRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

class PointServiceTest {

    private  final PointRepository pointRepository = mock(PointRepository.class);
    private final PointService pointService = new PointService(pointRepository);

    private double x = 123.234;
    private double y = 123.234;
    private String name = "Tom";
    private String color = "red";

    private double x1 = 34.234;
    private double y1 = 345.34;
    private String name1 = "Kled";
    private String color1 = "black";

    private Point point = new Point(x, y, name, color);
    private Point point1 = new Point(x1, y1, name1, color1);

    private List<Point> points = new ArrayList<>();

    @Test
    void testCreatePointsWhenListEmpty() {
        List<Point> points = new ArrayList<>();

        BadRequestException ex = assertThrows(BadRequestException.class, () ->
                pointService.createPoints(points));

        assertEquals(ex.getMessage(), "Points list is empty!");
    }

    @ParameterizedTest
    @CsvSource({
            "45.54, 456.8, fghj, ghj, 0",
            "0, 0, hj, nol, 0",
            "0, 456.8, fghj, ghj, 0",
            "789, 0, fghj, ghj, 0",
            "-789, 0, fghj, ghj, 1",
            "789, -890, fghj, ghj, 1",
            "-789, -890, fghj, ghj, 1",
            "-789, -890, null, ghj, 1",
            "-789, -890, null, null, 1",
            "-789, -890, fghj, null, 1"
    })
    public void testCreatePoints(Double x, Double y, String name, String color, int invalidAmount) {
        points.add(point);
        points.add(point1);
        points.add(new Point(x, y, name, color));

        CreatePointResponse response = pointService.createPoints(points);

        assertEquals(response.getSavedPointsCount(), 3 - invalidAmount);
        assertEquals(response.getInvalidPointsCount(), invalidAmount);
    }
}